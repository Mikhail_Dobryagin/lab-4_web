package com.example.web4.controllers;

import com.example.web4.dto.request.UserRequest;
import com.example.web4.dto.response.UserResponse;
import com.example.web4.entities.User;
import com.example.web4.exceptions.AlreadyExistException;
import com.example.web4.exceptions.InvalidPasswordException;
import com.example.web4.exceptions.NonExistentUserException;
import com.example.web4.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/sir")
    public String get() {
        return "SIR, YES SIR!";
    }

    @CrossOrigin
    @PostMapping(value = "/enter")
    public @ResponseBody UserResponse enter(@RequestBody UserRequest userRequest) {
        System.out.println(userRequest);
        UserResponse response = new UserResponse();
        try{
            response.withUid(userService.getUser(userRequest).getId());
            response.withStatus(0);
        }catch (NonExistentUserException ex)
        {
            return response.withStatus(1).withDescription(ex.getMessage());
        }catch (InvalidPasswordException ex) {
            return response.withStatus(3).withDescription(ex.getMessage());
        }

        return response;
    }

    @CrossOrigin
    @PostMapping(value = "/register")
    public @ResponseBody UserResponse register(@RequestBody UserRequest userRequest) {

        UserResponse response = new UserResponse();
        try{
            response.withUid(userService.register(userRequest.getName(), userRequest.getPassword()));
            response.withStatus(0);
        }catch (AlreadyExistException ex)
        {
            return response.withStatus(2).withDescription(ex.getMessage());
        }
        return response;
    }
}
