package com.example.web4.controllers;

import com.example.web4.dto.request.ShotRequest;
import com.example.web4.dto.response.ShotResponse;
import com.example.web4.services.ShotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("/shots")
public class ShotController {
    @Autowired
    ShotService shotService;

    @CrossOrigin
    @GetMapping
    public @ResponseBody
    List<ShotResponse> getAll(@RequestParam Long uid) {
        return shotService.getAll(uid).stream().map(ShotResponse::new).collect(Collectors.toList());
    }

    @CrossOrigin
    @PostMapping(value = "/shot")
    public @ResponseBody
    ShotResponse shot(@RequestBody ShotRequest shotRequest) {
        return shotService.validateShot(shotRequest) ? new ShotResponse(shotService.addShot(shotRequest)) : null;
    }

    @CrossOrigin
    @PostMapping(value = "/fewShots")
    public @ResponseBody
    List<ShotResponse> addShots(
            @RequestBody
                    List<ShotRequest> shotRequests) {
        boolean validationRes = true;
        for (ShotRequest shotRequest : shotRequests)
            validationRes = validationRes && shotService.validateShot(shotRequest);

        if (!validationRes)
            return null;

        return shotRequests.stream().map(shotRequest -> new ShotResponse(shotService.addShot(shotRequest))).collect(Collectors.toList());
    }


    @CrossOrigin
    @GetMapping("/clear")
    public @ResponseBody
    List<ShotResponse> clear(@RequestParam Long uid) {
        System.out.println(uid);
        shotService.clear(uid);
        return shotService.getAll(uid).stream().map(ShotResponse::new).collect(Collectors.toList());
    }
}
