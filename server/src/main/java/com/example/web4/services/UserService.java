package com.example.web4.services;

import com.example.web4.dao.UsersRepository;
import com.example.web4.dto.request.UserRequest;
import com.example.web4.entities.User;
import com.example.web4.exceptions.AlreadyExistException;
import com.example.web4.exceptions.InvalidPasswordException;
import com.example.web4.exceptions.NonExistentUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UsersRepository usersRepository;

    public Long register(String name, Long password) {
        if(usersRepository.existsByName(name))
            throw new AlreadyExistException("Такой пользователь уже существует");
        User user = new User(name, password);
        usersRepository.save(user);
        return user.getId();
    }

    public boolean check(UserRequest user) {
        return usersRepository.existsByNameAndPassword(user.getName(), user.getPassword());
    }

    public User getUser(UserRequest userRequest) {
        if(!hasUser(userRequest.getName()))
            throw new NonExistentUserException("Такого пользователя не существует");
        if(!check(userRequest))
            throw new InvalidPasswordException("Неверный пароль");

        return usersRepository.getByNameAndPassword(userRequest.getName(), userRequest.getPassword());
    }

    public boolean hasUser(String name) {
        return usersRepository.existsByName(name);
    }

    public void delete(String name) {
        usersRepository.deleteByName(name);
    }
}
