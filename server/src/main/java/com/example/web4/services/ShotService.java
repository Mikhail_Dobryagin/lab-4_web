package com.example.web4.services;

import com.example.web4.dao.ShotsRepository;
import com.example.web4.dto.request.ShotRequest;
import com.example.web4.entities.Shot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

@Service
public class ShotService {
    @Autowired
    ShotsRepository shotsRepository;

    public List<Shot> getAll(Long uid) {
        List<Shot> list = new LinkedList<>();
        shotsRepository.findAllByUid(uid).forEach(list::add);
        list.sort((s1, s2) -> s1.getId() > s2.getId() ? -1 : 1);
        return list;
    }

    public Shot addShot(ShotRequest shotRequest) {
        long startTime = System.nanoTime();
        String curTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
        boolean result = checkShot(shotRequest.getX(), shotRequest.getY(), shotRequest.getR());
        long execTime = System.nanoTime() - startTime;
        Shot newShot = new Shot(null, shotRequest.getX(), shotRequest.getY(), shotRequest.getR(), curTime, execTime, result, shotRequest.getUid());
        shotsRepository.save(newShot);
        return newShot;
    }

    public boolean checkShot(double x, double y, double r) {
        return
                x <= 0 && 0 <= y && y <= 2. * x + r ||
                        y <= 0 && x <= 0 && y * y + x * x <= r * r ||
                        0 <= x && x <= r && -r / 2. <= y && y <= 0;
    }

    public void clear(Long uid) {
        shotsRepository.deleteAllByUid(uid);
    }

    public boolean validateShot(ShotRequest shotRequest) {
        boolean flagX = true, flagR = false, flagY;

        for (double i = -4; i <= 4; i++) {
            if (shotRequest.getR() == i) {
                flagR = true;
                break;
            }
        }

        flagY = -5 <= shotRequest.getY() && shotRequest.getY() <= 5;

        return flagX && flagY && flagR;
    }
}
