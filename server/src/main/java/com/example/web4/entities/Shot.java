package com.example.web4.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "shots")
public class Shot {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "X", nullable = false)
    private double x;

    @Column(name = "Y", nullable = false)
    private double y;

    @Column(name = "R", nullable = false)
    private double r;

    @Column(name = "curTime", nullable = false)
    private String curTime;

    @Column(name = "execTime", nullable = false)
    private double execTime;

    @Column(name = "result", nullable = false)
    private boolean result;

    @Column(name = "UID", nullable = false)
    private Long uid;
}
