package com.example.web4.dto.response;

import com.example.web4.entities.Shot;
import lombok.Data;

@Data
public class ShotResponse {
    private double x, y, r, execTime;
    private String curTime;
    private boolean result;

    public ShotResponse(Shot shot) {
        x = shot.getX();
        y = shot.getY();
        r = shot.getR();
        curTime = shot.getCurTime();
        execTime = shot.getExecTime();
        result = shot.isResult();
    }
}
