package com.example.web4.dto.response;

import com.example.web4.entities.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserResponse {
    Integer status;
    String description;
    Long uid;

    public UserResponse(User user)
    {
        this.uid = user.getId();
    }

    public UserResponse withUid(Long uid) {
        this.uid = uid;
        return this;
    }

    public UserResponse withStatus(Integer status) {
        this.status = status;
        return this;
    }

    public UserResponse withDescription(String description) {
        this.description = description;
        return this;
    }
}
