package com.example.web4.dao;

import com.example.web4.entities.Shot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ShotsRepository extends CrudRepository<Shot, Long> {
    Iterable<Shot> findAllByUid(Long uid);
    @Transactional
    void deleteAllByUid(Long uid);
}
