package com.example.web4.dao;

import com.example.web4.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<User, Long> {
    boolean existsByName(String username);

    boolean existsByNameAndPassword(String username, Long password);

    User getByNameAndPassword(String name, Long password);

    void deleteByName(String name);
}
