import React, {useRef} from 'react';
import MainHeader from "../components/MainHeader";
import EnterForm from "../components/EnterForm";
import axios from "axios";
import '../styles/styles.css'
import {Link} from "react-router-dom";

const StartPage = ({setUid, ...props}) => {
    const pwdInputRef = useRef();

    const checkAuthorization = (response) => {
        if (isNaN(Number(response))) {
            alert(response);
            return;
        }
        let uid = Number(response)

        if (!isNaN(uid)) {
            setUid(uid);
            document.getElementById('toMainLink').click();
        }
    }
    const sF = (login, pwdText) => {
        if (!(isNaN(Number(window.localStorage.getItem('uid'))) || Number(window.localStorage.getItem('uid')) <= 0)) {
            checkAuthorization(Number(window.localStorage.getItem('uid')));
            return;
        }
        let pwd = 0
        for (let i = 0; i < pwdText.length; i++)
            pwd += pwdText.charCodeAt(i)

        const req = {
            name: login,
            password: pwd
        }

        axios.post("http://localhost:5555/user/enter", req).then((res) => {
            switch (res.data.status) {
                case 0:
                    checkAuthorization(res.data.uid);
                    break;
                case 1:
                    document.getElementById('login_field').style.border = '2px solid red';
                    setTimeout(() => document.getElementById('login_field').style.border = '', 2000);
                    break;
                case 3:
                    document.getElementById('pwd_field').style.border = '2px solid red';
                    setTimeout(() => document.getElementById('pwd_field').style.border = '', 2000);
                    break;

                default:
                    break;
            }
        });
    }

    const rF = (login, pwdText) => {
        let pwd = 0
        for (let i = 0; i < pwdText.length; i++)
            pwd += pwdText.charCodeAt(i)

        const req = {
            name: login,
            password: pwd
        }

        axios.post("http://localhost:5555/user/register", req).then((res) => {
            if (res.data.status === 0) {
                checkAuthorization(res.data);
                return;
            }
            document.getElementById('login_field').style.border = '2px solid red';
            setTimeout(() => document.getElementById('login_field').style.border = '', 2000);
        })
    }

    return (
        <table className='main_table'>

            <MainHeader variant={32488} name={'Добрягин Михаил Александрович'} group={'P3231'}/>

            <tbody>
            <tr>
                <td>
                    <div style={{display: 'none'}}>
                        <Link
                            to='/main'
                            // ref={(ref) => toMainRef = ref}
                            id='toMainLink'
                        >MAIN</Link>
                    </div>
                    <EnterForm onSend={sF} onRegister={rF} pwdInputRef={pwdInputRef}/>
                </td>
            </tr>
            </tbody>
        </table>
    );
};

export default StartPage;