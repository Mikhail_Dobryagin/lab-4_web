import React from 'react';
import MainHeader from "../components/MainHeader";
import {Link} from "react-router-dom";
import '../styles/styles.css'
import Graph from "../components/Graph";
import Clock from "../components/Clock";
import axios from "axios";

class MainPageCl extends React.Component {

    wrongRadiusAnimation = async () => {
        let element = document.getElementById('r_label');
        element.animate({
            opacity: 0
        }, 500);

        setTimeout(() => {
            element.style.color = 'red';

            element.animate({
                opacity: 1
            }, 500);

            setTimeout(() => {

                element.animate({
                    opacity: 0
                }, 2500);

                setTimeout(() => {
                    element.style.color = 'black';

                    element.animate({
                        opacity: 1
                    }, 500);
                }, 2500);

            }, 500);
        }, 500);
    }

    setUID = (val) => {
        this.state.UID = val;
        window.localStorage.setItem('uid', undefined);
        this.setState(this.state);
    }

    setR = (val) => {
        this.state.R = val;
        this.setState(this.state);
    }

    setPoints = (val) => {
        this.state.points = val;
        this.setState(this.state);
    }

    setY = (val) => {
        this.state.y = val;
        this.setState(this.state);
    }

    addOrClear = (event) => {
        if (event.target.value === 'Удалить всё') {

            axios.get('http://localhost:5555/shots/clear', {params: {uid: this.state.UID}}).then(res => {
                this.setPoints(res.data);
                // console.log(this.state.points);
            })
        } else {
            if (this.state.R.length === 0) {
                // alert('Радиус не задан');
                this.wrongRadiusAnimation();
                return;
            }

            if (isNaN(Number(this.state.y)))
                return;

            let requests = [];
            this.state.R.forEach(r => {
                if (r < 0) return;
                this.state.X.forEach(x => {
                    requests = [...requests, {uid: this.state.UID, r: r, x: x, y: this.state.y}]
                })
            })

            // console.log('requests');
            // console.log(requests);

            axios.post('http://localhost:5555/shots/fewShots', requests).then(res => {
                res.data.forEach(attempt => attempt.key = Date.now().toString() + res.data.indexOf(attempt));
                res.data.forEach(attempt => attempt.execTime = Number(attempt.execTime) / 1000);
                this.setPoints([...res.data, ...this.state.points]);
                // console.log(this.state.points);
            })
        }
    }

    clickX = (event) => {
        let cur = event.target.value;
        let x = this.state.X;

        if (event.target.checked === true) {
            x = [...x, cur]
            x.sort((x, y) => Number(x) < Number(y) ? -1 : 1);
        } else
            x.splice(x.indexOf(cur), 1);
        // console.log(x);

        let newState = this.state;
        newState.X = x;
        this.setState(newState);
    }
    clickR = (event) => {
        let cur = event.target.value;
        let r = this.state.R;

        if (event.target.checked === true) {
            r = [...r, cur]
            r.sort((x, y) => Number(x) < Number(y) ? -1 : 1);
        } else
            r.splice(r.indexOf(cur), 1);
        // console.log(r);

        let newState = this.state;
        newState.R = r;
        this.setState(newState);
    }

    constructor(props) {
        super(props);

        this.state = {
            UID: props['uid'],
            R: [],
            X: [],
            y: '',
            points: [],
            exitRef: React.createRef(),
        }

        axios.get('http://localhost:5555/shots', {params: {uid: this.state.UID}}).then(res => {
            let points = res.data;
            res.data.forEach(attempt => attempt.key = Date.now().toString() + res.data.indexOf(attempt));
            this.setPoints(points)
        });
    }


    // alert(UID);


    render() {
        if (isNaN(Number(this.state.UID)) || this.state.UID < 1)
            return <div></div>;
        // console.log(this.state.points);

        return (
            <table className='main_table'>

                <MainHeader variant={32488} name={'Добрягин Михаил Александрович'} group={'P3231'}/>

                <tbody>
                <tr className='input_tr'>
                    <td className='card_left' id='card_top'>
                        <div className="card_title table_title" style={{borderRadius: '20px 5px 0 0'}}>
                            Область
                            <Link
                                to="/start"
                                style={{display: 'none'}}
                                ref={this.state.exitRef}
                            >START</Link>
                        </div>

                        <div className="card_space">
                            <Graph
                                points={this.state.points}
                                R={this.state.R}
                                wrongRadiusAnimation={this.wrongRadiusAnimation}
                                getClickData={(data) => {
                                    data.forEach(point => point.uid = this.state.UID);
                                    axios.post('http://localhost:5555/shots/fewShots', data).then(res => {
                                        console.log(res);
                                        res.data.forEach(attempt => attempt.key = Date.now().toString() + res.data.indexOf(attempt));
                                        this.setPoints([...res.data, ...this.state.points]);
                                    });
                                }
                                }

                            />
                        </div>
                    </td>

                    <td rowSpan="2" className="card" id="card_right">
                        <div style={{borderRadius: '5px 20px 0 0'}} className="card_title table_title top_title">
                            Результаты
                        </div>

                        <div className="card_space">
                            <table className="result_table" style={{height: '5%'}}>
                                <thead>
                                <tr style={{background: 'WhiteSmoke'}}>
                                    <th className="input_data_column">X</th>
                                    <th className="input_data_column">Y</th>
                                    <th className="input_data_column">R</th>
                                    <th className="time_column">Время запуска</th>
                                    <th className="time_column">Время работы</th>
                                    <th className="result_column">ВЕРДИКТ</th>
                                </tr>
                                </thead>
                            </table>

                            <div style={{overflowX: 'auto', height: '95%'}}>
                                <table id="result_table_body" className="result_table">
                                    <tbody>
                                    {
                                        this.state.points.map(point =>
                                            <tr key={point.key}>
                                                <td className="input_data_column">{point.x}</td>
                                                <td className="input_data_column">{point.y}</td>
                                                <td className="input_data_column">{point.r}</td>
                                                <td className="time_column">{point.curTime}</td>
                                                <td className="time_column">{point.execTime}</td>
                                                <td className="result_column">
                                                    {
                                                        point.result ?
                                                            <img src='./tick.svg' alt='tick'/> :
                                                            <img src='./cross.svg' alt='cross'/>
                                                    }
                                                </td>
                                            </tr>
                                        )
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div style={{borderRadius: '0 0 20px 5px'}} className="card_title table_title bottom_title">
                            <Clock id='2'/>
                        </div>

                    </td>
                </tr>


                <tr>
                    <td className="card card_left" id="card_down">
                        <div className="card_space">
                            <form id="input_form">
                                <table id="input_table">

                                    <tbody>
                                    {/*X*/}
                                    <tr className="input_tr">
                                        <td className="input_label"><label id="x_label">X:</label></td>

                                        <td className="input_values">
                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox1" className="int_number">-4</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox1"
                                                       type="checkbox" name="x_input" value="-4"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox2" className="int_number">-3</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox2"
                                                       type="checkbox" name="x_input" value="-3"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox3" className="int_number">-2</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox3"
                                                       type="checkbox" name="x_input" value="-2"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox4" className="int_number">-1</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox4"
                                                       type="checkbox" name="x_input" value="-1"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox5" className="uint_number">0</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox5"
                                                       type="checkbox" name="x_input" value="0"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox6" className="uint_number">1</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox6"
                                                       type="checkbox" name="x_input" value="1"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox7" className="uint_number">2</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox7"
                                                       type="checkbox" name="x_input" value="2"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox8" className="uint_number">3</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox8"
                                                       type="checkbox" name="x_input" value="3"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="x_checkbox9" className="uint_number">4</label>
                                                <input onClick={(event) => this.clickX(event)} id="x_checkbox9"
                                                       type="checkbox" name="x_input" value="4"/>
                                            </div>

                                        </td>
                                    </tr>

                                    {/*Y*/}
                                    <tr className="input_tr">
                                        <td className="input_label">
                                            <label id="y_label" htmlFor="y_text">Y:</label>
                                        </td>

                                        <td className="input_div">
                                            <input type='text' placeholder='(-5;5)'
                                                   onChange={(event) => {
                                                       event.preventDefault();
                                                       let y = Number(event.target.value)

                                                       if (event.target.value !== '-' && (isNaN(y) || !(-5 <= y && y <= 5))) {
                                                           event.target.value = this.state.y;
                                                           return;
                                                       }
                                                       this.setY(event.target.value);
                                                   }}
                                                   value={this.state.y}
                                                   maxLength={5}
                                            />
                                        </td>
                                    </tr>

                                    {/*R*/}
                                    <tr className="input_tr">
                                        <td className="input_label"><label id="r_label">R:</label></td>

                                        <td className="input_values">
                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox1" className="int_number">-4</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox1"
                                                       type="checkbox" name="r_input" value="-4"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox2" className="int_number">-3</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox2"
                                                       type="checkbox" name="r_input" value="-3"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox3" className="int_number">-2</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox3"
                                                       type="checkbox" name="r_input" value="-2"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox4" className="int_number">-1</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox4"
                                                       type="checkbox" name="r_input" value="-1"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox5" className="uint_number">0</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox5"
                                                       type="checkbox" name="r_input" value="0"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox6" className="uint_number">1</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox6"
                                                       type="checkbox" name="r_input" value="1"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox7" className="uint_number">2</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox7"
                                                       type="checkbox" name="r_input" value="2"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox8" className="uint_number">3</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox8"
                                                       type="checkbox" name="r_input" value="3"/>
                                            </div>

                                            <div className="input_div checkbox_div">
                                                <label htmlFor="r_checkbox9" className="uint_number">4</label>
                                                <input onClick={(event) => this.clickR(event)} id="r_checkbox9"
                                                       type="checkbox" name="r_input" value="4"/>
                                            </div>

                                        </td>

                                    </tr>

                                    {/*Buttons*/}
                                    <tr>
                                        <td colSpan="2">
                                            <div className="buttons">
                                                <button id="submit_button" className="button" type="button"
                                                        onClick={(event) => this.addOrClear(event)}
                                                        value='Отправить'>Отправить
                                                </button>
                                                <button id="reset_button" className="button" type="reset"
                                                        onClick={event => this.addOrClear(event)}
                                                        value="Удалить всё">Удалить всё
                                                </button>

                                                <button className="button" type="button" value="Выйти"
                                                        onClick={() => {
                                                            this.setUID(undefined);
                                                            this.state.exitRef.current.click();
                                                        }}>Выйти
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div style={{borderRadius: '0 0 5px 20px'}} className="card_title table_title">
                            Значения
                        </div>

                    </td>
                </tr>

                </tbody>
            </table>
        );
    }
};

export default MainPageCl;