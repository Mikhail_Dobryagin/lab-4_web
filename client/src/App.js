import StartPage from "./pages/StartPage";
import './styles/styles.css';
import {Route} from "react-router";
import {HashRouter, Routes} from "react-router-dom";
import MainPageCl from "./pages/MainPageCl";
import MobileMainPageCl from "./pages/MobileMainPageCl";

function App() {

    const getUid = () => window.localStorage.getItem('uid');

    const WrappedStartPage = (props) => {
        // console.log(getUid());
        return (<StartPage {...props} setUid={uid => window.localStorage.setItem('uid', uid)}/>);
    }

    const WrappedMainPage = (props) => {
        // console.log(getUid());
        // console.log(window.outerWidth);
        return window.outerWidth >= 1265 ? (<MainPageCl {...props} uid={getUid()}/>) : (
            <MobileMainPageCl {...props} uid={getUid()}/>);
    }

    return (

        <HashRouter>
            <Routes>
                <Route path='/main' element={<WrappedMainPage/>}/>
                <Route path='/start' element={<WrappedStartPage/>}/>
                <Route path='/' element={<WrappedStartPage/>}/>
            </Routes>
        </HashRouter>
    );
}

export default App;
