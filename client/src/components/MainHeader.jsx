import React from 'react';
import '../styles/styles.css'

const MainHeader = ({name, group, variant}) => {
    return (
        <thead>
        <tr>
            <td colSpan="2"  className='main_header table_title'>
                <span style={{float: 'left'}}>{name} ({group})</span>
                <span style={{float: 'right'}}>{variant}</span>
            </td>
        </tr>
        </thead>
    );
};

export default MainHeader;