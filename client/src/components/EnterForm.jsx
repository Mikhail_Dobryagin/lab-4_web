import React, {useState} from 'react';
import Clock from "./Clock";
import '../styles/EnterForm.css'

const EnterForm = ({onSend, onRegister, pwdInputRef}) => {

    const changeName = (event) => {
        setName(event.target.value)
    }

    const changePwd = (event) => {
        setPwdText(event.target.value)
    }

    const [pwdText, setPwdText] = useState("")
    const [name, setName] = useState("")

    return (
        <div id='user_form_div'>
            <Clock id='1'/>
            <form id='user_form_inputs'>
                {
                    isNaN(Number(window.localStorage.getItem('uid'))) || Number(window.localStorage.getItem('uid')) <= 0 ?
                        <input type="text" id='login_field' value={name} onChange={event => changeName(event)}/> : ''
                }
                {
                    isNaN(Number(window.localStorage.getItem('uid'))) || Number(window.localStorage.getItem('uid')) <= 0 ?
                        <input type="password" id='pwd_field' ref={pwdInputRef} value={pwdText}
                               onChange={event => changePwd(event)}/> : ''
                }
                <button onClick={(event) => {
                    event.preventDefault();
                    onSend(name, pwdText);
                }}>Продолжить
                </button>
                {
                    isNaN(Number(window.localStorage.getItem('uid'))) || Number(window.localStorage.getItem('uid')) <= 0 ?
                        <button onClick={(event) => {
                            event.preventDefault();
                            onRegister(name, pwdText);
                        }}>Зарегистрироваться</button> : ''
                }

            </form>
        </div>
    );
};

export default EnterForm;