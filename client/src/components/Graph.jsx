import React, {useRef} from 'react';
import '../styles/styles.css'

const Graph = ({points, R, getClickData, wrongRadiusAnimation}) => {

    const graphRef = useRef()
    const onClick = (event) => {

        let xPage = event.pageX - graphRef.current.getBoundingClientRect().left;
        let yPage = event.pageY - graphRef.current.getBoundingClientRect().top;

        let data = [];


        R.forEach(newR => {
            if (newR <= 0)
                return;
            let newX = ((xPage - 110) * newR / (176 - 110)).toFixed(2);
            let newY = ((110 - yPage) * newR / (176 - 110)).toFixed(2);

            data = [...data, {x: newX, y: newY, r: newR}]
        })

        if(data.length === 0)
        {
            wrongRadiusAnimation()
            return;
        }
        getClickData(data);
    }

    return (
        <svg ref={graphRef} id="graph" width="220" height="220" className="svg-graph"
             onClick={(event => onClick(event))}>

            {/*X-Axis*/}
            <line className="axis" x1="10" y1="110" x2="210" y2="110" stroke="black"/>
            <polygon points="210,110 200,105 200,115"/>
            {/*Y-Axis*/}
            <line className="axis" x1="110" y1="10" x2="110" y2="210" stroke="black"/>
            <polygon points="110,10 105,20 115,20"/>

            {/*X-Axis coordinates*/}
            <line className="coordinate-line" x1="43" y1="105" x2="43" y2="115" stroke="black"/>
            <text className="coordinate-text" x="33" y="100" fontSize="14">-R</text>

            <line className="coordinate-line" x1="76" y1="105" x2="76" y2="115" stroke="black"/>
            <text className="coordinate-text" x="66" y="100" fontSize="14">-R/2</text>

            <line className="coordinate-line" x1="143" y1="105" x2="143" y2="115" stroke="black"/>
            <text className="coordinate-text" x="138" y="100" fontSize="14">R/2</text>

            <line className="coordinate-line" x1="176" y1="105" x2="176" y2="115" stroke="black"/>
            <text className="coordinate-text" x="171" y="100" fontSize="14">R</text>

            {/*Y-Axis coordinates*/}
            <line className="coordinate-line" x1="105" y1="176" x2="115" y2="176" stroke="black"/>
            <text className="coordinate-text" x="120" y="181" fontSize="14">-R</text>

            <line className="coordinate-line" x1="105" y1="143" x2="115" y2="143" stroke="black"/>
            <text className="coordinate-text" x="120" y="148" fontSize="14">-R/2</text>

            <line className="coordinate-line" x1="105" y1="76" x2="115" y2="76" stroke="black"/>
            <text className="coordinate-text" x="120" y="81" fontSize="14">R/2</text>

            <line className="coordinate-line" x1="105" y1="43" x2="115" y2="43" stroke="black"/>
            <text className="coordinate-text" x="120" y="48" fontSize="14">R</text>

            {/*Triangle*/}
            <polygon className="svg-figure triangle-figure" points="110,110 110,43 76,110"
                     fill="blue" fillOpacity="0.25" stroke="darkblue" strokeOpacity="0.5"/>

            {/*Circle*/}
            <path className="svg-figure circle-figure"
                  d="M 43 110 A 66 66 0 0 0 110 176 L 110 110 Z"
                  fill="blue" fillOpacity="0.25" stroke="darkblue" strokeOpacity="0.5"/>

            {/*Rectangle*/}
            <polygon className="svg-figure rectangle-figure"
                     points="110,143 110,110 176,110 176,143"
                     fill="blue" fillOpacity="0.25" stroke="darkblue" strokeOpacity="0.5"/>
            {
                points !== undefined ? points.map(point =>
                <circle key={point.key} className='point' fill={point.result ? '#08E72AFF' : '#ff5454'}
                        fillOpacity='0.5' stroke='#003850' strokeWidth='1'
                        cx={(point.x * (176. - 110.) / point.r + 110.).toFixed(2).toString()}
                        cy={(110. - point.y * (176. - 110.) / point.r).toFixed(2).toString()} r='2'/>
            ) : ''}
        </svg>
    );
};

export default Graph;