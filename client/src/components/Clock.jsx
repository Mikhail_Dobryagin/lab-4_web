import React from 'react';
class Clock extends React.Component {
    id;
    runClock = (setTime) => {
        setInterval(() => {
            let curDate = new Date(Date.now());
            setTime((curDate.getHours() < 10 ? "0" : "") + curDate.getHours() + ":" + (curDate.getMinutes()<10 ? "0" : "") + curDate.getMinutes());
        }, 11000);
    }

    constructor(props) {
        super(props);
        this.id = props['id'];
        let curDate = new Date(Date.now());
        this.state = {
            time: (curDate.getHours() < 10 ? "0" : "") + curDate.getHours() + ":" + (curDate.getMinutes()<10 ? "0" : "") + curDate.getMinutes()
        }
        this.runClock(newTime => {this.setState({time: newTime})});
    }

    render() {
        return (
                <span id={'clock'+this.id}>{this.state.time}</span>
        ) ;
    }
}

export default Clock;